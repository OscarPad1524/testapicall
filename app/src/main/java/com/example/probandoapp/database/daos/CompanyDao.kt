package com.example.probandoapp.database.daos

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.probandoapp.database.models.AddressTable
import com.example.probandoapp.database.models.CompanyTable

@Dao
interface CompanyDao {

    @Query("SELECT * FROM company_table")
    fun getAll(): List<CompanyTable>


    @Insert
    fun insertAll(vararg company: CompanyTable)

    @Delete
    fun delete(company: CompanyTable)

}