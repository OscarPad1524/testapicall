package com.example.probandoapp.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "company_table")
data class CompanyTable (
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "name")var name:String = "",
    @ColumnInfo(name = "catch_phrase") var catchPhrase:String = "",
    @ColumnInfo(name = "bs")var bs:String = ""
)

