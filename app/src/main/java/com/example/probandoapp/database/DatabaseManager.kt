package com.example.probandoapp.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.probandoapp.database.daos.AddressDao
import com.example.probandoapp.database.daos.CompanyDao
import com.example.probandoapp.database.daos.PostDao
import com.example.probandoapp.database.daos.UsersDao
import com.example.probandoapp.database.models.AddressTable
import com.example.probandoapp.database.models.CompanyTable
import com.example.probandoapp.database.models.PostTable
import com.example.probandoapp.database.models.UsersTable

@Database(
    entities = [
        UsersTable::class,
        AddressTable::class,
        CompanyTable::class,
        PostTable::class
    ],
    version = 1
)
abstract class DatabaseManager: RoomDatabase() {

        abstract fun usersDao(): UsersDao
        abstract fun addressDao(): AddressDao
        abstract fun companyDao(): CompanyDao
        abstract fun postDao(): PostDao

        companion object {
            @Volatile
            private var INSTANCE: DatabaseManager? = null

            @JvmStatic
            fun getDatabase(context: Context): DatabaseManager {
                val tempInstance = INSTANCE

                if (tempInstance != null) {
                    return tempInstance
                }

                synchronized(this) {
                    val instance = Room.databaseBuilder(
                        context.applicationContext,
                        DatabaseManager::class.java, "AppDatabase.db"
                    ).build()

                    INSTANCE = instance

                    return instance
                }
            }
        }

}