package com.example.probandoapp.database.daos

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.probandoapp.database.models.AddressTable
import com.example.probandoapp.database.models.UsersTable

@Dao
interface AddressDao {

    @Query("SELECT * FROM addrees_table")
    fun getAll(): List<AddressTable>


    @Insert
    fun insertAll(vararg address: AddressTable)

    @Delete
    fun delete(address: AddressTable)
}