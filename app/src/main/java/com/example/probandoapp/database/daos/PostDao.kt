package com.example.probandoapp.database.daos

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.probandoapp.database.models.PostTable
import com.example.probandoapp.database.models.UsersTable

@Dao
interface PostDao {
    @Query("SELECT * FROM post_table")
    fun getAll(): List<PostTable>

    @Query("SELECT * FROM post_table WHERE user_id=:id LIMIT 1")
    fun getById(id: Long): PostTable

    @Query("SELECT * FROM post_table WHERE user_id=:id")
    fun getAllById(id: Long): List<PostTable>

    @Query("SELECT * FROM post_table WHERE id IN (:userIds)")
    fun loadAllByIds(userIds: IntArray): List<PostTable>

    //@Query("SELECT * FROM post_table WHERE name LIKE :name AND " +
    //        "username LIKE :userName LIMIT 1")
    //fun findByName(name: String, userName: String): PostTable

    @Insert
    fun insert(user: PostTable): Long

    //@Insert
    //fun insertAll(vararg users: UsersTable)

    @Delete
    fun delete(user: PostTable)
}