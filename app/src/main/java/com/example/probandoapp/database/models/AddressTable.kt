package com.example.probandoapp.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.android.gms.maps.model.LatLng

@Entity(tableName = "addrees_table")
data class AddressTable (

    @PrimaryKey val id: Int,
    @ColumnInfo(name = "street") var street: String = "",
    @ColumnInfo(name = "suite")var suite: String = "",
    @ColumnInfo(name = "city")var city: String = "",
    @ColumnInfo(name = "zipcode")var zipcode: String = "",
    @ColumnInfo(name = "geoLocal")var geo: String? = ""

)
