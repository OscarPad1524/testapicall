package com.example.probandoapp.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "users_table",
//foreignKeys = [
//    ForeignKey(entity = AddressTable::class, parentColumns = ["id"], childColumns = ["id_address"]),
//    ForeignKey(entity = CompanyTable::class, parentColumns = ["id"], childColumns = ["id_company"])
//]
)

data class UsersTable(



    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") val id: Long = 0,
    @ColumnInfo(name = "id_remote") var id_server: String? = "",
    @ColumnInfo(name = "name") var name: String? = "",
    @ColumnInfo(name = "username") var username: String? = "",
    @ColumnInfo(name = "email") var email: String? = "",
    @ColumnInfo(name = "id_address") var id_address: Int? = 0,
    @ColumnInfo(name = "phone") var phone: String? = "",
    @ColumnInfo(name = "web_site") var website: String? = "",
    @ColumnInfo(name = "id_company") var company: Int? = 0


)