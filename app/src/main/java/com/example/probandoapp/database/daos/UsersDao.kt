package com.example.probandoapp.database.daos

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.probandoapp.database.models.UsersTable

@Dao
interface UsersDao {

    @Query("SELECT * FROM users_table")
    fun getAll(): List<UsersTable>

    @Query("SELECT * FROM users_table WHERE id=:id")
    fun getById(id: Long): UsersTable

    @Query("SELECT * FROM users_table WHERE id IN (:userIds)")
    fun loadAllByIds(userIds: IntArray): List<UsersTable>

    @Query("SELECT * FROM users_table WHERE name LIKE :name AND " +
            "username LIKE :userName LIMIT 1")
    fun findByName(name: String, userName: String): UsersTable

    @Insert
    fun insert(user: UsersTable): Long

    //@Insert
    //fun insertAll(vararg users: UsersTable)

    @Delete
    fun delete(user: UsersTable)

}