package com.example.probandoapp.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "post_table")
data class PostTable(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") val id: Long = 0,
    @ColumnInfo(name = "body") val body: String = "",
    @ColumnInfo(name = "id_remote") val id_remote: Int = 0,
    @ColumnInfo(name = "tittle") val title: String = "",
    @ColumnInfo(name = "user_id") val userId: Int = 0
)