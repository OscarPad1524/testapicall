package com.example.probandoapp

import com.example.probandoapp.UserDataCollectionItem
import android.telecom.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface UserService {

    @GET("users")
    fun listUsers(): retrofit2.Call<List<UserDataCollectionItem>>

    @GET("posts")
    fun listPost(): retrofit2.Call<List<PostItem>>

    @GET("posts?userId={id}")
    fun listPostByUser(@Path("id") id:Int, @Body post: PostItem?): retrofit2.Call<List<PostItem>>

}