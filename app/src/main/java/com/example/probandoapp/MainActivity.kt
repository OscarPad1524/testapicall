package com.example.probandoapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.probandoapp.adapter.PostAdapter
import com.example.probandoapp.adapter.UsersAdapter
import com.example.probandoapp.database.DatabaseManager
import com.example.probandoapp.database.models.PostTable
import com.example.probandoapp.database.models.UsersTable
import com.example.probandoapp.databinding.ActivityMainBinding
import com.example.probandoapp.retrofit.RestEngine
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var dataBaseManager: DatabaseManager
    private lateinit var adaptadorPost: PostAdapter
    private lateinit var adaptadorUser: UsersAdapter
    private lateinit var lista: List<UsersTable>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        dataBaseManager = DatabaseManager.getDatabase(this)



        initRecycler()

            GlobalScope.launch {
                if (dataBaseManager.usersDao().getAll().size < 2) {
                    callServiceGetUsers()


                } else {
                    withContext(Dispatchers.Main) {
                        Toast.makeText(this@MainActivity, "Datos ya ingresados a base de datos local", Toast.LENGTH_SHORT).show()

                    }
                }
                lista = dataBaseManager.usersDao().getAll()
                if (lista.size < 2) {
                    callServiceGetPost()
                } else {

                    setLista(lista)

                }
            }










    }

    private fun initRecycler() {
        binding.recycler.layoutManager = LinearLayoutManager(this)
        adaptadorUser = UsersAdapter(this){ usersTable, i ->
            onClick(usersTable, i)
        }
        binding.recycler.adapter = adaptadorUser
    }

    fun onClick(userTable: UsersTable, posicion: Int) {
        var intent:Intent = Intent(this, UsersEmptyActivity::class.java)
        intent.putExtra("id", userTable.id)
        startActivity(intent)
    }

    private fun setLista(lista: List<UsersTable>) {

        adaptadorUser.setLista(lista)

    }


    private fun callServiceGetUsers() {

        val userService: UserService = RestEngine.getRestEngine().create(UserService::class.java)
        val result: retrofit2.Call<List<UserDataCollectionItem>> = userService.listUsers()

        var respuesta: List<UserDataCollectionItem>? = listOf()


        result.enqueue(object : retrofit2.Callback<List<UserDataCollectionItem>>{
            override fun onFailure(
                call: retrofit2.Call<List<UserDataCollectionItem>>,
                t: Throwable
            ) {

            }

            override fun onResponse(
                call: retrofit2.Call<List<UserDataCollectionItem>>,
                response: Response<List<UserDataCollectionItem>>
            ) {
                respuesta = response.body()

                GlobalScope.launch {
                    for (i in respuesta!!) {

                        var user = UsersTable(
                            id = 0,
                            id_server = i.id.toString(),
                            name = i.name,
                            username = i.username,
                            email = i.email,
                            phone = i.phone,
                            website = i.website

                        )

                        dataBaseManager.usersDao().insert(user)

                        withContext(Dispatchers.Main) {


                            Toast.makeText(this@MainActivity, "Agregado ${i.name}", Toast.LENGTH_SHORT).show()
                        }


                    }
                }

            }
        }

        )


    }

    private fun callServiceGetPost() {

        val userService: UserService = RestEngine.getRestEngine().create(UserService::class.java)
        val result: retrofit2.Call<List<PostItem>> = userService.listPost()

        var respuesta: List<PostItem>? = listOf()

        result.enqueue(object : retrofit2.Callback<List<PostItem>>{
            override fun onFailure(call: retrofit2.Call<List<PostItem>>, t: Throwable) {
                Toast.makeText(this@MainActivity, "Ha ocurrido un error", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: retrofit2.Call<List<PostItem>>,
                response: Response<List<PostItem>>
            ) {

                respuesta = response.body()

                GlobalScope.launch {
                    for (i in response.body()!!) {

                        var post = PostTable(
                            id = 0,
                            body = i.body,
                            title = i.title,
                            id_remote = i.id,
                            userId = i.userId,

                        )

                        dataBaseManager.postDao().insert(post)



                    }
                }


                Toast.makeText(this@MainActivity, "Datos Extraidos", Toast.LENGTH_LONG).show()
                GlobalScope.launch {
                    lista = dataBaseManager.usersDao().getAll()
                    withContext(Dispatchers.Main) {
                        setLista(lista)
                    }

                }

            }
        })

    }


}