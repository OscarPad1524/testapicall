package com.example.probandoapp.repository

import android.telecom.RemoteConnection
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import com.example.probandoapp.UserDataCollectionItem
import com.example.probandoapp.UserService
import com.example.probandoapp.database.DatabaseManager
import com.example.probandoapp.database.daos.UsersDao
import com.example.probandoapp.database.models.UsersTable
import com.example.probandoapp.retrofit.RestEngine
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Response

class UsersRepository(private val apikey:String, private val usersDao:UsersDao) {
    private lateinit var databaseManager: DatabaseManager



    fun getAll(): List<UsersTable> {


        var result = listOf<UsersTable>()

        if (usersDao.getAll().isEmpty()) {

            var user = UsersTable()

            var response = callServiceGetUsers()?.forEach {
                i -> user.email = i.email
                user.name = i.name
                user.phone = i.phone
                user.username = i.username
                user.website = i.website

                GlobalScope.launch {
                    databaseManager.usersDao().insert(user)
                }
            }
            result = databaseManager.usersDao().getAll()


        } else {

            result = databaseManager.usersDao().getAll()

        }

        return result
    }

    private fun callServiceGetUsers(): List<UserDataCollectionItem>? {

        val userService: UserService = RestEngine.getRestEngine().create(UserService::class.java)
        val result: retrofit2.Call<List<UserDataCollectionItem>> = userService.listUsers()

        lateinit var respuesta: Response<List<UserDataCollectionItem>>


        result.enqueue(object : retrofit2.Callback<List<UserDataCollectionItem>>{
            override fun onFailure(
                call: retrofit2.Call<List<UserDataCollectionItem>>,
                t: Throwable
            ) {

            }

            override fun onResponse(
                call: retrofit2.Call<List<UserDataCollectionItem>>,
                response: Response<List<UserDataCollectionItem>>
            ) {
                respuesta = response
            }
        }

        )

        return respuesta.body()

    }

}