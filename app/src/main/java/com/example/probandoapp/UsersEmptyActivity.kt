package com.example.probandoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.probandoapp.adapter.PostAdapter
import com.example.probandoapp.database.DatabaseManager
import com.example.probandoapp.database.models.PostTable
import com.example.probandoapp.database.models.UsersTable
import com.example.probandoapp.databinding.ActivityMainBinding
import com.example.probandoapp.databinding.ActivityUsersEmptyBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UsersEmptyActivity : AppCompatActivity() {


    private var idString:Long = 0
    private var user = UsersTable()
    private var idRemota = PostTable()
    private var listPost: List<PostTable> = listOf()
    private lateinit var adapter:PostAdapter
    private lateinit var binding: ActivityUsersEmptyBinding
    private lateinit var databaseManager: DatabaseManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityUsersEmptyBinding.inflate(layoutInflater)
        setContentView(binding.root)
        databaseManager = DatabaseManager.getDatabase(this)


        idString = intent.extras?.getLong("id")!!

        GlobalScope.launch {

            user = databaseManager.usersDao().getById(idString)
            idRemota = databaseManager.postDao().getById(user.id_server!!.toLong())
            listPost = databaseManager.postDao().getAllById(idRemota.id)

            withContext(Dispatchers.Main) {
                Toast.makeText(this@UsersEmptyActivity, "Binvenido ${user.id}", Toast.LENGTH_SHORT).show()
                binding.tvEmail.setText(user.email)
                binding.tvUsername.setText(user.username)
                binding.tvNombre.setText(user.name)
                binding.tvPhone.setText(user.phone)
                binding.website.setText(user.website)

                initRecycler()
                setLista(listPost)
            }

        }






    }

    private fun initRecycler() {
        binding.recyclerPosts.layoutManager = LinearLayoutManager(this)
        adapter = PostAdapter(this)
        binding.recyclerPosts.adapter = adapter
    }

    private fun setLista(lista: List<PostTable>) {

        adapter.setLista(lista)

    }
}