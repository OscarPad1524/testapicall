package com.example.probandoapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.probandoapp.R
import com.example.probandoapp.database.models.PostTable

class PostAdapter(

    var context: Context
): RecyclerView.Adapter<PostAdapter.PostHolder>() {

    private var post: List<PostTable> = listOf()

    inner class PostHolder(val view: View): RecyclerView.ViewHolder(view) {

        var tvTittle: TextView = view.findViewById(R.id.tvTittle)
        var tvBody: TextView = view.findViewById(R.id.tvBody)

        fun render(post: PostTable, context: Context) {
            tvTittle.text = post.title
            tvBody.text = post.body

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return PostHolder(layoutInflater.inflate(R.layout.post_item, parent, false))
    }

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        holder.render(post[position], context)
    }

    override fun getItemCount(): Int = post.size

    fun setLista(lista: List<PostTable>) {
        post = lista
        notifyDataSetChanged()
    }

}