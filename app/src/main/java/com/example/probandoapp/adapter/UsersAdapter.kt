package com.example.probandoapp.adapter

import android.content.Context
import android.service.autofill.OnClickAction
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.probandoapp.R
import com.example.probandoapp.database.models.PostTable
import com.example.probandoapp.database.models.UsersTable

class UsersAdapter(
    val context: Context,
    var onClic: (UsersTable, Int) -> Unit
): RecyclerView.Adapter<UsersAdapter.UsersHolder>() {

    private var users: List<UsersTable> = listOf()

    inner class UsersHolder (val view:View): RecyclerView.ViewHolder(view) {

        var tvName:TextView = view.findViewById(R.id.tvUser)

        fun render(user: UsersTable, context: Context) {
            tvName.text = user.name

            itemView.setOnClickListener() {

                onClic(user, adapterPosition)

            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersAdapter.UsersHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return UsersHolder(layoutInflater.inflate(R.layout.user_item, parent, false))
    }

    override fun onBindViewHolder(holder: UsersAdapter.UsersHolder, position: Int) {
        holder.render(users[position], context)
    }

    override fun getItemCount(): Int = users.size

    fun setLista(lista: List<UsersTable>) {
        users = lista
        notifyDataSetChanged()
    }
}