package com.example.probandoapp.retrofit

import com.example.probandoapp.UserDataCollectionItem
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RestEngine {



    companion object {

        fun getRestEngine():Retrofit {

            val retrofit = Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()


            return retrofit
        }

    }

}